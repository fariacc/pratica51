package utfpr.ct.dainf.if62c.pratica;
/**
 * Representa uma matriz de valores {@code double}.
 * @author Wilson Horstmeyer Bogadao <wilson@utfpr.edu.br>
 */
public class Matriz {
    
    // a matriz representada por esta classe
    private final double[][] mat;
    
    /**
     * Construtor que aloca a matriz.
     * @param m O número de linhas da matriz.
     * @param n O número de colunas da matriz.
     * @throws utfpr.ct.dainf.if62c.pratica.MatrizInvalidaException
     * 
     */
    public Matriz(int m, int n) throws MatrizInvalidaException {
        mat = new double[m][n];        
        if(m <= 0 || n <= 0)
            throw new MatrizInvalidaException(m, n);
    }
    
    /**
     * Retorna a matriz representada por esta classe.
     * @return A matriz representada por esta classe
     */
    public double[][] getMatriz() {
        return mat;
    }
    
    /**
     * Retorna a matriz transposta.
     * @return A matriz transposta.
     * @throws utfpr.ct.dainf.if62c.pratica.MatrizInvalidaException
     */
    public Matriz getTransposta() throws MatrizInvalidaException {
        Matriz t = new Matriz(mat[0].length, mat.length);
        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat[i].length; j++) {
                t.mat[j][i] = mat[i][j];
            }
        }
        return t;
    }
    
    /**
     * Retorna a soma desta matriz com a matriz recebida como argumento.
     * @param m A matriz a ser somada
     * @return A soma das matrizes
     * @throws utfpr.ct.dainf.if62c.pratica.MatrizInvalidaException
     * @throws utfpr.ct.dainf.if62c.pratica.SomaMatrizesIncompativeisException
     */
    public Matriz soma(Matriz m) throws MatrizInvalidaException, SomaMatrizesIncompativeisException {
        double[][] _m = m.getMatriz();
        int linhas = mat.length, colunas = mat[0].length;
        
        if(_m.length != linhas || _m[0].length != colunas)
            throw new SomaMatrizesIncompativeisException(this, m);

        Matriz resultante = new Matriz(linhas, colunas);
        double[][] _resultante = resultante.getMatriz();

        for(int i = 0 ; i < linhas ; i++)
            for(int j = 0 ; j < colunas ; j++)
                _resultante[i][j] = ( mat[i][j] + _m[i][j] );
        
        return resultante;
    }

    /**
     * Retorna o produto desta matriz com a matriz recebida como argumento.
     * @param m A matriz a ser multiplicada
     * @return O produto das matrizes
     * @throws utfpr.ct.dainf.if62c.pratica.MatrizInvalidaException
     * @throws utfpr.ct.dainf.if62c.pratica.ProdMatrizesIncompativeisException
     */
    public Matriz prod(Matriz m) throws MatrizInvalidaException, ProdMatrizesIncompativeisException {
        double[][] _m = m.getMatriz();
        int linhasA = mat.length, colunasA = mat[0].length;
        int linhasB = _m.length, colunasB = _m[0].length;
        
        if(colunasA != linhasB)
            throw new ProdMatrizesIncompativeisException(this, m);

        Matriz resultante = new Matriz(linhasA, colunasB);
        double[][] _resultante = resultante.getMatriz();

        for(int i = 0 ; i < linhasA ; i++)
            for(int j = 0 ; j < colunasB ; j++)
                _resultante[i][j] = getMultiplicacaoParaPosicao(mat, _m, i, j, linhasB);
        
        return resultante;
    }

    private double getMultiplicacaoParaPosicao(double[][] mA, double[][] mB, int numLinha, int numColuna, int quant) {
        double soma = 0.0;
        for (int i = 0; i < quant; i++) {
            soma += mA[numLinha][i] * mB[i][numColuna];
        }
        return soma;
    }

    /**
     * Retorna uma representação textual da matriz. Este método não deve ser
     * usado com matrizes muito grandes pois não gerencia adequadamente o
     * tamanho do string e poderia provocar um uso excessivo de recursos.
     *
     * @return Uma representação textual da matriz.
     */
    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        for (double[] linha : mat) {
            s.append("[ ");
            for (double x : linha) {
                s.append(x).append(" ");
            }
            s.append("]");
        }
        return s.toString();
    }

}