package utfpr.ct.dainf.if62c.pratica;

public class MatrizInvalidaException extends Exception {
    public final int linha, coluna;
    
    public MatrizInvalidaException(int linha, int coluna) {
        super("Matriz de" + linha + "x" + coluna + " nao pode ser criada"); 
        this.linha = linha;
        this.coluna = coluna;
    }
    
    public int getNumLinhas() {
        return linha;
    }
    public int getNumColunas() {
        return coluna;
    }
}
