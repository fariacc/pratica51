package utfpr.ct.dainf.if62c.pratica;

public class MatrizesIncompativeisException extends RuntimeException {
    public MatrizesIncompativeisException(String message) {
        super(message);
    }
}

