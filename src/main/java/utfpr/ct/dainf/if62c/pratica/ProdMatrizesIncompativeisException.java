package utfpr.ct.dainf.if62c.pratica;

public class ProdMatrizesIncompativeisException extends MatrizesIncompativeisException {
    public final Matriz m1;
    public final Matriz m2;
    
    public ProdMatrizesIncompativeisException(Matriz m1, Matriz m2) {
        super(String.format("Matrizes de dimensÃµes %dx%d e %dx%d nÃ£o podem ser multiplicadas", m1.getMatriz().length, m1.getMatriz()[0].length, m2.getMatriz().length, m2.getMatriz()[0].length));
        
        this.m1 = m1;
        this.m2 = m2;
    }
}
