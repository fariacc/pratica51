import utfpr.ct.dainf.if62c.pratica.Matriz;
import utfpr.ct.dainf.if62c.pratica.MatrizInvalidaException;
import utfpr.ct.dainf.if62c.pratica.ProdMatrizesIncompativeisException;
import utfpr.ct.dainf.if62c.pratica.SomaMatrizesIncompativeisException;

public class Pratica51 {

    public static void main(String[] args) throws MatrizInvalidaException, ProdMatrizesIncompativeisException, SomaMatrizesIncompativeisException {
        Matriz orig = new Matriz(3, 2);
        double[][] m = orig.getMatriz();
        Matriz transp = orig.getTransposta();
        System.out.println("Matriz original: " + orig);
        System.out.println("Matriz transposta: " + transp);

        Matriz nova = new Matriz(3, 2);
         double[][] _nova = nova.getMatriz();
         _nova[0][0] = 1.0;
         _nova[0][1] = 2.0;
         _nova[1][0] = 3.0;
         _nova[1][1] = 4.0;
         _nova[2][0] = 5.0;
         _nova[2][1] = 6.0;
         Matriz somada = orig.soma(nova);
         System.out.println("Matriz nova: " + nova);
         System.out.println("Matriz somada: " + somada);       
        
         orig = new Matriz(3, 2);
         m = orig.getMatriz();
         m[0][0] = 2.0;
         m[0][1] = 3.0;
         m[1][0] = 0.0;
         m[1][1] = 1.0;
         m[2][0] = -1.0;
         m[2][1] = 4.0;        
         nova = new Matriz(2, 3);
         _nova = nova.getMatriz();
         _nova[0][0] = 1.0;
         _nova[0][1] = 2.0;
         _nova[0][2] = 3.0;
         _nova[1][0] = -2.0;
         _nova[1][1] = 0.0;
         _nova[1][2] = 4.0;
         Matriz multiplicada = orig.prod(nova);
         System.out.println("Matriz original: " + orig);
         System.out.println("Matriz nova: " + nova);
         System.out.println("Matriz multiplicada AxB: " + multiplicada);
         multiplicada = nova.prod(orig);
         System.out.println("Matriz multiplicada BxA: " + multiplicada);
    }
}
